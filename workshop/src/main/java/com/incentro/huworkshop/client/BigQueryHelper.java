package com.incentro.huworkshop.client;

import com.google.api.client.extensions.appengine.http.UrlFetchTransport;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.bigquery.Bigquery;
import com.google.api.services.bigquery.model.TableDataInsertAllRequest;
import com.google.api.services.bigquery.model.TableRow;

import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;

/**
 * @author Zeger Hoogeboom
 */
public class BigQueryHelper
{

	/*
	This "service account" (which operates on behalf of a service instead of a user) belongs to a project of ours.
	Since you need billing enabled for BigQuery (BQ) you're directly inserting into OUR BQ.

	The combination of the "key.p12" and the service account lets you have access to it.
	 */
	private static final String SERVICE_ACCOUNT = "1028142913494-697d2rn1ausn8cjhilh7iultv4nr72o0@developer.gserviceaccount.com";

	/*
	Every action you want to do within ANY google API requires you to request a scope.
	The "bigquery" scope is in this case granted by the "service account".
	"Regular" web applications ask the user for their permission to access scopes on behalf of the user, like accessing their gmail/drive.
	 */
	private static final String SCOPE = "https://www.googleapis.com/auth/bigquery";
	private static final HttpTransport TRANSPORT = new UrlFetchTransport();
	private static final JsonFactory JSON_FACTORY = new JacksonFactory();

	private static final String BQ_PROJECT_ID = "huworkshop-bq";
	private static final String BQ_DATASET_ID = "workshopdata";
	private static final String BQ_TABLE_ID = "pageviews";

	public static Bigquery auth() throws GeneralSecurityException, IOException
	{
		/*
		GoogleCredential is a generically usable credential, not only for BigQuery (it just requires different scopes).
		e.g. If we would add the scope "https://www.googleapis.com/auth/devstorage.read_write" we would be able to access Google Cloud Storage
		 */
		GoogleCredential credential = new GoogleCredential.Builder().setTransport(TRANSPORT)
				.setJsonFactory(JSON_FACTORY)
				.setServiceAccountId(SERVICE_ACCOUNT)
				.setServiceAccountScopes(Collections.singletonList(SCOPE))
				.setServiceAccountPrivateKeyFromP12File(new File("key.p12"))
				.build();

		return new Bigquery.Builder(TRANSPORT, JSON_FACTORY, credential)
					.setApplicationName("BigQuery-Service-Accounts/0.1")
					.setHttpRequestInitializer(credential).build();
	}

	public static void insertRow(String projectId, Long pageViews) throws IOException, GeneralSecurityException
	{
		Bigquery bigquery = BigQueryHelper.auth();

		TableRow row = new TableRow();
		row.set("projectid", projectId);
		row.set("pageviews", pageViews);

		TableDataInsertAllRequest.Rows rows = new TableDataInsertAllRequest.Rows();
		rows.setInsertId(new Date().toString());
		rows.setJson(row);

		TableDataInsertAllRequest content = new TableDataInsertAllRequest().setRows(Arrays.asList(rows));
		bigquery.tabledata().insertAll(BQ_PROJECT_ID, BQ_DATASET_ID, BQ_TABLE_ID, content).execute();
	}
}
