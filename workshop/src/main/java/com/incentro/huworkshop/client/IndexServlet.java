package com.incentro.huworkshop.client;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Zeger Hoogeboom
 */
public class IndexServlet extends HttpServlet
{
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		/*
        Check if user is logged in:
            https://cloud.google.com/appengine/docs/java/users/

        Create a new GuestbookEntry()

        Save it to the Datastore with Objectify (while not being required, Objectify makes your life a whole lot easier):
            Datastore: https://cloud.google.com/appengine/docs/java/datastore/
            Objectify: https://github.com/objectify/objectify/wiki
		 */
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		/*
		List all guestbook entries
		In the App Engine tutorial (https://cloud.google.com/appengine/docs/java/gettingstarted/ui_and_code)
		data to be displayed is requested in the JSP. This is fine for a quick-and-dirty workshop but ofcourse not the way to go "in real life".

		To change this, you can either do request.setAttribute("comments", commentsList) or pre-process the HTML with a templating engine like Mustache.
		see here for an example with Mustache: https://github.com/icoloma/cloudpresident/blob/master/src/com/me/president/ThePresidentServlet.java
		(ask us if you want more explaining/help)
		 */
	}
}
