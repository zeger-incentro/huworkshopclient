package com.incentro.huworkshop.client;

import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;
import com.google.apphosting.api.ApiProxy;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.GeneralSecurityException;

/**
 * @author Zeger Hoogeboom
 */
public class InsertPageviewServlet extends HttpServlet
{

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		String projectId = getProjectId();
		MemcacheService memcacheService = MemcacheServiceFactory.getMemcacheService();

		/*
		Insert a record in Memcache (see https://cloud.google.com/appengine/docs/java/memcache/)
		This will look like {"your-project-id":2} and will increase every time this method gets called
		 */
		Long increment = memcacheService.increment(projectId, 1l, 0L);

		/*
		To avoid going over "burst quota", only insert into BigQuery every 200 request
		 */
		if (increment >= 200) {
			memcacheService.delete(projectId);
			try {
				BigQueryHelper.insertRow(projectId, increment);
			} catch (GeneralSecurityException e) {
				e.printStackTrace();
				throw new ServletException(e.getMessage());
			}
		}
		request.setAttribute("projectid", projectId);
		request.getRequestDispatcher("/index.jsp").forward(request, response);
	}

	/*
	Uses the "App Identity" service, which is a service much like the Memcache
	see https://cloud.google.com/appengine/docs/java/appidentity/
	 */
	private static String getProjectId() {
		return ApiProxy.getCurrentEnvironment().getAppId();
	}
}
